// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/SpeedBonus.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSpeedBonus() {}
// Cross Module References
	SNAKE_API UClass* Z_Construct_UClass_ASpeedBonus_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_ASpeedBonus();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Snake();
	SNAKE_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ASpeedBonus::StaticRegisterNativesASpeedBonus()
	{
	}
	UClass* Z_Construct_UClass_ASpeedBonus_NoRegister()
	{
		return ASpeedBonus::StaticClass();
	}
	struct Z_Construct_UClass_ASpeedBonus_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASpeedBonus_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASpeedBonus_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "SpeedBonus.h" },
		{ "ModuleRelativePath", "SpeedBonus.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ASpeedBonus_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ASpeedBonus, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASpeedBonus_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASpeedBonus>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASpeedBonus_Statics::ClassParams = {
		&ASpeedBonus::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASpeedBonus_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASpeedBonus_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASpeedBonus()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASpeedBonus_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASpeedBonus, 125641570);
	template<> SNAKE_API UClass* StaticClass<ASpeedBonus>()
	{
		return ASpeedBonus::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASpeedBonus(Z_Construct_UClass_ASpeedBonus, &ASpeedBonus::StaticClass, TEXT("/Script/Snake"), TEXT("ASpeedBonus"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASpeedBonus);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
